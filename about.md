---
layout: page
email: hello@samwilkinson.io
title: About
cta: Work
permalink: /about
---
I moved to New York in 2018 to join the team at [Petal](https://www.petalcard.com/), where I help build and deploy our underwriting, fraud, and marketing models.

I’ve previously worked in data science and ML at NASA and the European Space Agency. Most recently, I helped set up the data science team at [Nested](https://nested.com/). Before moving into data science, I studied Physics at Oxford.

You can find a copy of my resume [here](https://drive.google.com/open?id=1zlEKCiIEBkEQ21cN8M51p0hIotp9Cch9).

## Not Work

I grew up in the UK, but my parents are Canadian so I never picked up much of an English accent (even after 20 years). 

Like many people, being quarantined has focused my love of baking.